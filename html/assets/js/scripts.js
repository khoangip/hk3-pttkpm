(function($) {

    $(document).ready(function() {
        mobileMenu();
        slider();
        backToTop();
        elevatezoom();
        productDetail();
    });

    function mobileMenu() {
        $('.navbar-toggle').mobileMenu({
            targetWrapper: '.block-main-menu',
        });
    }

    function productDetail() {
        $('.block-navbartabs .block-header li a').smoothScroll({
            speed: 600
        });
    };

    function slider() {
        $('#owlcarousel-3 .view-content').owlCarousel({
            loop:false,
            margin:15,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $('.view-carousel .view-content').owlCarousel({
            loop:false,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                350:{
                    items:2
                },
                600:{
                    items:3
                },
                1000:{
                    items:3
                }
            }
        });

        $('#owlcarousel-5 .view-content').owlCarousel({
            loop:false,
            margin:20,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                350:{
                    items:2
                },
                600:{
                    items:3
                },
                1000:{
                    items:5
                }
            }
        });
        
        $('#owlcarousel-10 .view-content').owlCarousel({
            loop:false,
            margin:10,
            nav:true,
            responsive:{
                0:{
                    items:2
                },
                350:{
                    items:3
                },
                600:{
                    items:5
                },
                1000:{
                    items:10
                }
            }
        })
    };

    function backToTop() {
        $(window).load(function () {
            if($(this).scrollTop()){
                $('.btn-btt').fadeIn();
            }
            else{
                $('.btn-btt').fadeOut();
            }
        });
        $(window).scroll(function () {
            if($(this).scrollTop()){
                $('.btn-btt').fadeIn();
            }
            else{
                $('.btn-btt').fadeOut();
            }
        });

        $('.btn-btt').click(function () {
            $('html, body').animate({scrollTop : 0}, 700);
            return false;
        })
    }

    function elevatezoom() {
        $("#img_01").elevateZoom({
            gallery:'gal1',
            cursor: 'pointer',
            galleryActiveClass: 'active',
            imageCrossfade: true
        }); 

        //pass the images to Fancybox
        $("#img_01").bind("click", function(e) {  
        var ez =   $('#img_01').data('elevateZoom');	
            $.fancybox(ez.getGalleryList());
        return false;
        });
    }

})(jQuery);
